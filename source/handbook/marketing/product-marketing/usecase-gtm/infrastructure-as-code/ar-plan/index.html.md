---
layout: markdown_page
title: "UseCase XYZ AR Plan"
---

Overall AR plan for the <XYZ> use case

| Firm | Gartner | Forrester | IDC | Other |
|--------------------|------------------------------|-----------|-----|-------|
| Key Analysts | <list key analysts> |  |  |  |
| Recent research | <links to relevant research> |  |  |  |
| Briefing frequency | <monthly, quarterly, etc> |  |  |  |
| Briefing Agenda Doc | <link to the briefing G-Doc> |  |  |  |
