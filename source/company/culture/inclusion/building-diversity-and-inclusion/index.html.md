---
layout: markdown_page
title: "Building and Sustaining a Diverse and Inclusive Culture"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Introduction

On this page, we are detailing how to build and sustain a Diverse and Inclusive Culture in a remote environment. 

## How do you build and sustain culture in a remote environment?

There are so many elements to consider when building a Diverse and Inclusive culture.

## Areas to consider
Evaluating the unique landscape of your company
Naming of D&I
Creating ERGs (Employee Resource Groups)?
Creating a D&I Advisory Group?
D&I Initiatives?
D&I Awards for Recognition?
D&I Surveys?
D&I Strategy?
D&I Framework?
D&I Training for all levels?
Inclusive Benefits?
D&I Engagement?

