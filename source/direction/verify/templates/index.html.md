---
layout: markdown_page
title: "Category Direction - Templates"
---

- TOC
{:toc}

## Templates

Templates make it easy to setup a new project by starting from an existing one that already has all the required configuration, files, and boilerplate. GitLab provides a variety of templates as a starting point for creating new projects and it's also possible to contribute to these; administrators can even configure templates specific to your GitLab instance.

We already have dozens of templates for common implementations: [Project templates](https://gitlab.com/gitlab-org/project-templates), [CI YAML templates](https://gitlab.com/gitlab-org/gitlab/tree/master/lib/gitlab/ci/templates), [Pages templates](https://gitlab.com/pages), and even [example projects](https://gitlab.com/gitlab-examples). Don't see a template that you wish we had? Please let us know! Contact [Thao Yeager](https://gitlab.com/thaoyeager), the PM for this category.

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name%5B%5D=Category%3ATemplates)
- [Overall Vision](/direction/cicd)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/2287)
- Documentation: [Project templates](https://docs.gitlab.com/ee/gitlab-basics/create-project.html#project-templates), [Custom group-level project templates](https://docs.gitlab.com/ee/user/group/custom_project_templates), [CI YAML templates](https://docs.gitlab.com/ee/ci/examples/README.html#cicd-examples)

## What's Next & Why

Along the theme of [doing powerful things easily](/direction/cicd/#do-powerful-things-easily), we want to [make ML/AI pipelines easier to setup and use](https://gitlab.com/groups/gitlab-org/-/epics/2436). This includes adding new templates for creating [KubeFlow](https://gitlab.com/gitlab-org/gitlab/issues/197142) and [PyTorch](https://gitlab.com/gitlab-org/gitlab/issues/200040) projects based on common implementations for those emerging technologies.

Aside from creating new templates, we will also improve our existing ones. One example is updating our [Node.js template to automatically upload packages to the NPM registry](https://gitlab.com/gitlab-org/gitlab/issues/10050); this simplifies building and deploying packages for Javascript developers using GitLab’s NPM Registry. We also plan to improve the quality of CI YAML templates and make updates as needed so that options like the [Maven .gitlab-ci.yml template](https://gitlab.com/gitlab-org/gitlab/issues/27393) work as expected.

## Maturity Plan

This category is currently at the “Viable” maturity level, and our next maturity target is Complete (see our [definitions of maturity levels](/direction/maturity/)). Key deliverables to achieve this are:

- [Add CI/CD template for KubeFlow workflows](https://gitlab.com/gitlab-org/gitlab/issues/197142)
- [Add CI/CD template for PyTorch workflows](https://gitlab.com/gitlab-org/gitlab/issues/200040)
- [Update node.js template to automatically push to the NPM registry](https://gitlab.com/gitlab-org/gitlab/issues/10050)
- [Setup Package Registry integrations with Templates](https://gitlab.com/gitlab-org/gitlab/issues/29120)
- [Provide Terraform templates for CI/CD](https://gitlab.com/gitlab-org/gitlab/issues/32485)
- [Copy CI variables with custom project templates](https://gitlab.com/gitlab-org/gitlab/issues/7317)
- [Parameterize Project Templates](https://gitlab.com/gitlab-org/gitlab/issues/26580)
- [Document how to vendor Project Templates](https://gitlab.com/gitlab-org/gitlab/issues/22145)

## Competitive Landscape

### GitHub

GitHub offers ["Starter Workflows"](https://github.com/actions/starter-workflows) to get started with GitHub Actions. These are [templates for popular continuous integration workflows written in YAML](https://github.com/actions/starter-workflows/tree/master/ci). The YAML file is dependent on a corresponding .properties.json file that defines the metadata about the workflow. 

### Jenkins

Jenkins' concept of ["pipeline template"](https://jenkinsci.github.io/templating-engine-plugin/pages/Pipeline_Templating/what_is_a_pipeline_template.html) involves two other components, libraries and configuration files. The pipeline template defines "stages" which depend on "libraries" to specify the jobs to be executed for each stage, while the configuration file specifies which libraries are used. Reusable templates are consolidated and managed under a governance structure using [Jenkins Templating Engine (JTE)](https://jenkinsci.github.io/templating-engine-plugin/), a templating plugin.

### CircleCI

CircleCI provides sample configuration files ([Sample 2.0 config.yml Files](https://circleci.com/docs/2.0/sample-config/)) to guide the user, along with CircleCI Orbs' importable jobs as building blocks for creating a configuration file.

## Top Customer Success/Sales Issue(s)

There are currently no top CS/Sales issues for this category.

## Top Customer Issue(s)

Our most popular customer issue is adding the ability to copy CI variables for custom project templates ([gitlab#7317](https://gitlab.com/gitlab-org/gitlab/issues/7317)). Without the missing CI variables, the project is not ready to be used and the manual customizations to add the variables is prone to errors. 

Of similar concern on the topic of variables is a specific customer request to parameterize project templates ([gitlab#26580](https://gitlab.com/gitlab-org/gitlab/issues/26580)) to enable project templates configured with variables to prompt the user for inputs that will eliminate manual customizations after the project is created.

## Top Internal Customer Issue(s)

Our top internal customer issues include the following:

- Allow specifying environment variables for new templates ([gitlab#26979](https://gitlab.com/gitlab-org/gitlab/issues/26979))
- Allow specifying one-time script for new templates ([gitlab#26980](https://gitlab.com/gitlab-org/gitlab/issues/26980))
- Improve the workflow for creating a vendor template ([gitlab#22145](https://gitlab.com/gitlab-org/gitlab/issues/22145)) 

## Top Vision Item(s)

Our most important vision items are making ML/AI pipelines easier to setup/use ([gitlab#2436](https://gitlab.com/groups/gitlab-org/-/epics/2436)) by adding templates for KubeFlow projects ([gitlab#19742](https://gitlab.com/gitlab-org/gitlab/issues/197142)) and PyTorch projects ([gitlab#200040](https://gitlab.com/gitlab-org/gitlab/issues/200040). In addition, we want to improve our existing templates for CI YAML files and common project types.
