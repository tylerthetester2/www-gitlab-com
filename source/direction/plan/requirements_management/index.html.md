---
layout: markdown_page
title: "Category Direction - Requirements Management"
---

- TOC
{:toc}

## Requirements Management
<!-- A good description of what your category is. If there are
special considerations for your strategy or how you plan to prioritize, the
description is a great place to include it. -->

|                       |                                 |
| -                     | -                               |
| Stage                 | [Plan](/direction/plan/)        |
| Maturity              | [Planned](/direction/maturity/) |
| Content Last Reviewed | `2020-02-23`                    |

### Introduction and how you can help

Welcome to the category strategy for Requirements Management, part of the Plan stage's [Requirements Management](/handbook/product/categories/#certify-group) group. To provide feedback or ask questions, please reach out to the group's Product Manager, Mark Wood ([E-mail](mailto:mwood@gitlab.com)).

We believe in a world where **everyone can contribute**. We value your contributions, so here are some ways to join in!

[issues](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3ARequirements%20Management) and [epics](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3ARequirements%20Management)!
* Feel free to share feedback directly via email, Twitter or on a video call.
* Don't hesitate to create an issue for a new feature or enhancement, if one does not already exist.
* Finally, please create an MR against this direction page to make it better!

### Overview

The strategy for requirements management within GitLab is to reduce the friction associated with requirements management by providing an intuitive way of creating requirements and allowing these requirements, as well as other existing artifacts within the DevOps platform, to be linked together to enable traceability consistent with the needs of our diverse users.

#### What is Requirements Management?

It is often necessary to specify behaviors for a system or application. Requirements Management is a process by which these behaviors would be captured so that there is a clearly defined scope of work. A good general overview is provided in an [article from PMI](https://www.pmi.org/learning/library/requirements-management-planning-for-success-9669).

Requirements management tools are often prescriptive in their process, requiring users to modify their workflows to include traceability.

#### Aerospace Use Case

Regulated industries often have specific standards which define their development life-cycle. For example, commercial software-based aerospace systems must adhere to [RTCA DO-178C, Software Considerations in Airborne Systems and Equipment Certification](https://en.wikipedia.org/wiki/DO-178C). While this document covers all phases of the software development life cycle, the concept of traceability (defined as a documented connection) is utilized throughout. This connection must exist between the certification artifacts.

The most common trace paths needed are as follows:

* Software Allocated System Level Requirements <- High Level Software Requirements (HLR) <- Low Level Software Requirements (LLR) / Software Design <- Source Code <- Executable Object Code
* Software High Level & Low Level Requirements <- Test Cases <- Test Procedures <- Test Results

It is important to recognize that all artifacts must be under revision control.

During audits, teams are asked to demonstrate traceability from the customer specification through all downstream, version-controlled artifacts. Teams are often asked to analyze a change in a system level requirement, assessing exactly which downstream artifacts will need to be modified based on that change.

#### Other Regulated Industries

Further research is underway to determine if other regulated industries are similar in their use case for Requirements Management.

#### Key Terms / Concepts

**Requirements Decomposition** - It is up to the developers and architects to decompose (break down) high level requirements into many smaller low level requirements. All of these decomposed requirements would generally trace up to the high level requirement, thus forming a one-to-many (HLR to LLR) relationship.

**Derived Requirements** - Because regulated industries often require that all functionality within the software trace to a requirement, it is often necessary to create requirements at the LLR / Design level. These requirements, that were not decomposed from a higher level requirement, are called Derived Requirements.

**Traceability Matrix** - A common artifact that is often required is a traceability matrix. This is a released document which shows all traceability links in the system / sub-system.


### What's next & why
<!-- This is almost always sourced from the following sections, which describe top
priorities for a few stakeholders. This section must provide a link to an issue
or [epic](https://about.gitlab.com/handbook/product/#epics-for-a-single-iteration) for the MVC or first/next iteration in
the category.-->

We are currently in the early stages of implementing Requirements Management within GitLab. We recognize that GitLab is in a unique position to deliver an integrated Requirements Management solution since linking to all aspects (version controlled files, test procedures, etc...) could be accomplished without the need for external tools. This would allow our solution to effectively link to all necessary artifacts within a single product offering.

The first step of this category is building out the initial requirements structure, in particular, at the group level. This would allow a basic tree-structure of requirements. This would allow us to achieve [minimum maturity](https://gitlab.com/groups/gitlab-org/-/epics/1696) for this category.

Future iterations include integration/linking with artifacts within GitLab such as with test cases and test results, to achieve traceability. These iterations will be working toward achieving [viable maturity](https://gitlab.com/groups/gitlab-org/-/epics/1697) for this category.

Also related to this category is [Release Governance](/direction/release/secrets_management) from the [Release](/direction/release/), in particular the concepts related to evidence & artifact collection.

The below image illustrates an initial node diagram of the requirements definition.

![Requirements Node Diagram](requirements_mind_map.png)

### Long term goals

* We want to make sure requirements can be managed at all relevant levels, allowing there to be multiple projects which all would trace up to system level requirements stored within their own project.
* Similar to code files and test results, requirements are considered artifacts. We will strive to allow requirements to be managed and controlled in a manner similar to other artifacts, including version control and potentially merge requests.
* Visual representation of traceability and test coverage. We would like to provide a visual representation of ancestors and descendants of requirements, making it easy to visualize decomposition and traceability. It would also be ideal for passing / failing test results to roll up visually to the requirements, allowing for quick visualization of the requirement status with regards to implementation and verification.


### Competitive landscape
<!-- The top two or three competitors, and what the next one or two items we should
work on to displace the competitor at customers, ideally discovered through
[customer meetings](https://about.gitlab.com/handbook/product/#customer-meetings). We’re not aiming for feature parity
with competitors, and we’re not just looking at the features competitors talk
about, but we’re talking with customers about what they actually use, and
ultimately what they need.-->

Top competitors in this area are traditional tools that do requirements management used by business analysts, managers, and similar personas. Jama Connect and IBM Rational DOORS are two popular tools. Both of these tools offer limited integration with version control systems, making linking to necessary artifacts cumbersome. How GitLab can set ourselves apart is by reducing the need for outside tools, thus eliminating much of the friction associated with tracing artifacts to requirements.

### Analyst landscape
<!-- What analysts and/or thought leaders in the space talking about, and how we stay
relevant from their perspective.-->

We have yet to engage more closely with analysts in this area. As this product category is prioritized for improvements as our Plan product and engineering headcount grows, we expect to engage more with analysts.

### Top Customer Success/Sales issue(s)
<!-- These can be sourced from the CS/Sales top issue labels when available, internal
surveys, or from your conversations with them.-->

The first step is building out the requirements management structure to achieve [minimum maturity](https://gitlab.com/groups/gitlab-org/-/epics/1696).

### Top user issue(s) & Epics
<!-- This is probably the top popular issue from the category (i.e. the one with the most
thumbs-up), but you may have a different item coming out of customer calls.-->

| Issue / Epic                                                                                                                      | 👍 |
| -                                                                                                                                 | -  |
| [MVC: Allow users to create requirements](https://gitlab.com/groups/gitlab-org/-/epics/2703)                                      | 10 |
| [Multi-level epic and issue relationships tree-view as requirements management](https://gitlab.com/gitlab-org/gitlab/issues/7021) | 8  |
| [Requirements Management: Approve and Baseline](https://gitlab.com/gitlab-org/gitlab/issues/8461)                                 | 3  |

### Top internal customer issue(s)
<!-- These are sourced from internal customers wanting to [dogfood](/handbook/values/#dogfooding)
the product.-->

Currently, the quality department is [experimenting with requirements management](https://gitlab.com/gitlab-org/quality/team-tasks/issues/187). Further collaboration is necessary to understand their needs and whether or not our implementation is useful for their efforts.

## Top Vision Item(s)
<!-- What's the most important thing to move your vision forward?-->
- MVC: [Requirements at the Group Level](https://gitlab.com/groups/gitlab-org/-/epics/707)

## Related links
* [Requirements Management Initial Discussion](https://youtu.be/Ci3FTjYrwrA)
